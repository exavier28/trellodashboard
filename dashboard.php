<?php
	include ('config.php');
	include ('lib.php');

	if (!isset($_GET['quadroid'])){
			$quador_id = $quadroid[0];
			$trello = get_trello_data($quador_id);
	}elseif($_GET['quadroid'] == 'todos'){
			$quador_id = 'todos';
			$trello = get_trello_todos_data($quadroid);
	}else{
			$quador_id = $_GET['quadroid'];
			$trello = get_trello_data($quador_id);		
	}

	//echo "<pre>"; print_r($trello);echo "</pre>";
	
	$ntarefas = conta_tarefas($trello);
	$esforco = trellodashboar_out_esforco_googlechart($trello);
	$esforco_medida = trellodashboar_out_esforco_medida($trello);


?>
<head>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/style_menu_responsivo.css">
	
	
	
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<script type="text/javascript">
	  google.charts.load('current', {'packages':['corechart','bar']});
	  google.charts.setOnLoadCallback(drawChart);

	  function drawChart() {

		var data1 = google.visualization.arrayToDataTable([
		  ['Atividades', 'Checklist'],
		  ['Completo',     <?php echo  $ntarefas['complete']; ?>],
		  ['Incompleto',     <?php echo  $ntarefas['incomplete']; ?>]
		]);

		var options1 = {
		  title: 'Andamento do Quantitativo: <?php echo get_nome_quadro($quador_id); ?> ',
		  is3D: true,
		};

		var chart1 = new google.visualization.PieChart(document.getElementById('piechart'));

		chart1.draw(data1, options1);
		
		
		
		var data2 = google.visualization.arrayToDataTable([
		  ['Pessoa', 'Total(<?php echo $esforco_medida; ?>)', 'Completo(<?php echo $esforco_medida; ?>)', 'Incompleto(<?php echo $esforco_medida; ?>)'],
		  <?php
		    echo $esforco;
		  ?>
		]);

		var options2 = {
		  chart: {
			title: 'Acompanhamento de Esforço: <?php echo get_nome_quadro($quador_id); ?> ',
		  }
		};

		var chart2 = new google.charts.Bar(document.getElementById('columnchart_material'));

		chart2.draw(data2, options2);
		
		
	  }
	</script>

	
	
</head>

<style>
[data-tooltip] {
  position: relative;
}

[data-tooltip]:after {
  display: none;
  position: absolute;
  botton: -5px;
  padding: 5px;
  border-radius: 3px;
  left: calc(5px);
  content: attr(data-tooltip);
  background-color: #0095ff;
  color: #000;
  width:98%;
  height: auto;
  border: 2px solid #000;
}

[data-tooltip]:hover:after {
  display: block; 
}
</style>

 

<?php
	echo "<div class='abas'>";
	    echo "<header>";
			echo "<h1 class='float-l'>";
			echo "<a href='#' title='".get_nome_quadro($quador_id)."'>".get_nome_quadro($quador_id)."</a>";
			echo "</h1>";
			
			echo "<input type='checkbox' id='control-nav' />";
			echo "<label for='control-nav' class='control-nav'></label>";
			echo "<label for='control-nav' class='control-nav-close'></label>";
		
			echo "<nav class='float-r'>";
				echo "<ul class='list-auto'>";
				echo "<li><a href='index.php?quadroid=todos' >Todos</a></li>";
				foreach ($quadroid as $quadro){
					if ($quadro == $quador_id){$active = 'active';}else{$active = '';}
					echo "<li class='".$active."'>";
					echo "<a href='index.php?quadroid=".$quadro."' >".get_nome_quadro($quadro)."</a>";
					echo "</li>";
					
					
				}
				echo "</ul>";
			echo "<nav class='float-r'>";
		echo "</header>";
	echo "</div>";


	echo "<div class='content' style=''>";
	echo 	"<div class='trello_graficos'>";
	echo 		"<div id='piechart' style='width: 100%; min-height: 300px;'></div>";
	echo 		"<div id='columnchart_material' style='width: 100%; min-height: 300px;'></div>";
	echo 	"</div>";
	echo 	"<div class='trello_lista'>";
	echo 	"<div class='trello_lista_formatos'>Formatos:";
	echo 		"<a href='?quadroid=".$quador_id."&formato=quadros'> Quadros</a> | ";
	echo 		"<a href='?quadroid=".$quador_id."&formato=prioridades'>Prioridades</a>";
	echo 	"</div>";
	
		if ( (!isset($_GET['formato'])) or ($_GET['formato'] == 'prioridades') ){
			echo 		output_trello_viwer2($trello);
		}elseif($_GET['formato'] = 'quadros'){
			echo 		output_trello_viwer($trello);
		}
		
	echo 	"</div>";
	echo "</div>";
?>	
