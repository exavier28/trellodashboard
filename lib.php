<?php


function output_trello_viwer($trello){
	
	//$out = $out ."<div class='trello_quadro'> |->". $trello['quadro']['name']."</div>";
	foreach ($trello['quadro']['cartoes'] as $cartoes_array){
		foreach($cartoes_array as $cartoes){
			$out = $out ."<div class='trello_cartoes formato_quadros'>  <div class='cartao_title'>". $cartoes['name']."</div><div class='cartao_content'>";
			foreach($cartoes['checklist'] as $checklist ){
				$out = $out ."<div class='trello_checklist'> <div class='trello_checklist_title'>". $checklist['name']."</div>";
				if (count($checklist['list'])!=0){
					foreach($checklist['list'] as $list){
						//$out = $out ."<div class='trello_list'>  |----->". $list->name."</div>";	
							foreach($list->checkItems as $listItem){
								$out = $out ."<div class='trello_check_".$listItem->state."'><div></div><span title='".$listItem->name."'>". substr( $listItem->name,0,100)."</span></div>";
							}
					}
				}else{
								$out = $out ."<div class='trello_check_none'><div></div><span>Sem atividade definida</span></div>";
				}
				$out = $out . "</div>";
			}
			$out = $out . "</div></div>";
		}
	}
	return $out;
}


function output_trello_viwer2($trello){
	
	foreach ($trello['quadro']['cartoes'] as $quadro_id=>$array_cartoes){
		$nome_cartao = get_nome_quadro($quadro_id);
		foreach ($array_cartoes as $cartoes){
			foreach($cartoes['checklist'] as $checklist ){
				if (count($checklist['list'])!=0){
					foreach($checklist['list'] as $list){
							foreach($list->checkItems as $listItem){
								$prioridade = trellodashboar_get_prioridade($listItem->name);
								if ($listItem->state == 'complete'){$prioridade = 'P4';}
								if ($_GET['quadroid']=='todos'){
									$listItem_out[$prioridade][$listItem->state][] = "<b>".$nome_cartao ."&nbsp; -	&nbsp;". $checklist['name']."-".$list->name."	 -	".$checklist['nome']."</b>	&nbsp; -	&nbsp;".$listItem->name;
								}else{
									$listItem_out[$prioridade][$listItem->state][] = "<b>". $cartoes['name']."&nbsp; -	&nbsp;".$checklist['name']."-".$list->name."</b>	&nbsp;-&nbsp;".$listItem->name;
								}
							}
					}
				}
			}
		}
	}
	ksort($listItem_out);
		
	$out = $out ."<div class='trello_quadro formato_prioridades'> "; 
	
	foreach($listItem_out as $prioridade=>$array_state){
			$out = $out ."<div class='trello_cartoes'>";
			$out = $out . "<div class='cartao_title'>". trellodashboar_legenda_prioridade($prioridade)."</div><div class='cartao_content'>";
				asort($array_state);
				foreach($array_state as $state=>$listItem_array){
					foreach($listItem_array as $listItem_value){
						$out = $out ."<div class='trello_check_".$state."'><div></div><span title='".$listItem_value."'>". $listItem_value ."</span></div>";
					}
				}
			$out = $out ."</div>";
			$out = $out . "</div>";
	}
	$out = $out . "</div>";
	
	return $out;
}


function get_trello_data($quador_id){
	include ('config.php');


		$url = "https://api.trello.com/1/boards/".$quador_id."?lists=open&list_fields=name&fields=name,desc&key=".$key."&token=".$token." ";
		$quadros = trellodashboard_get_json_data($url);
		//echo "<pre>"; print_r($quadros); echo "</pre>";


		$trello['quadro']['name'] = $quadros->name;
		$trello['quadro']['id'] = $quadros->id;

		$n=0;
		foreach ($quadros->lists as $list){
			$list_out = trello_get_list($key,$token,$list->id);
			//echo "<pre>"; print_r($list_out); echo "</pre><hr>";

			$trello['quadro']['cartoes'][$quadros->id][$n]['name'] = $list_out->name;
			$trello['quadro']['cartoes'][$quadros->id][$n]['id'] = $list->id;
			
			$x=0;
			foreach ($list_out->cards as $card){
				
				//echo "<pre>"; print_r($card); echo "</pre><hr>"; 
				
				$checklists_out = trello_get_checklist($key,$token,$card->id);	
				//echo "<pre>"; print_r($checklists_out); echo "</pre><hr>";
				$trello['quadro']['cartoes'][$quadros->id][$n]['checklist'][$x]['name']	=$card->name;
				$trello['quadro']['cartoes'][$quadros->id][$n]['checklist'][$x]['list']	= (array)$checklists_out;
				
				$x=$x+1;
			}
			$n=$n+1;
		}
	return $trello;
}

function get_trello_todos_data($quadroid){
	$cartoes_out = array();
	foreach ($quadroid as $quadro_id){
		$trello = get_trello_data($quadro_id);
		//$cartoes_out = $trello['quadro']['cartoes'];
		$cartoes_out = array_merge($cartoes_out, $trello['quadro']['cartoes']);
	}
		$trello_out['quadro']['name'] = "Todos";
		$trello_out['quadro']['id'] = "todos";
		$trello_out['quadro']['cartoes'] = $cartoes_out;
		
	//echo "<pre>"; print_r($trello_out);echo "</pre>";
	return $trello_out;
}

function trello_get_board($key,$token,$id_board){
	$url = "https://api.trello.com/1/boards/".$id_board."?lists=open&list_fields=name&fields=name,desc&key=".$key."&token=".$token." ";
	$card = trellodashboard_get_json_data($url);
	return $card;	
}

function trello_get_list($key,$token,$id_list){
	$url = "https://api.trello.com/1/lists/".$id_list."?fields=name&cards=open&card_fields=name&key=".$key."&token=".$token." ";
	$list = trellodashboard_get_json_data($url);
	return $list;
}

function trello_get_checklist($key,$token,$id_card){
	$url = "https://api.trello.com/1/cards/".$id_card."/checklists?fields=name&cards=all&card_fields=name&key=".$key."&token=".$token." ";
	$checklist = trellodashboard_get_json_data($url);
	return $checklist;	
}

function conta_tarefas($trello){
	
	foreach ($trello['quadro']['cartoes'] as $cartoes_array){
		foreach($cartoes_array as $cartoes){
			foreach($cartoes['checklist'] as $checklist ){
				foreach($checklist['list'] as $list){
					foreach($list->checkItems as $listItem){
						$ncontatarefas['total'] = $ncontatarefas['total']+1; 
						if ($listItem->state == 'complete'){$ncontatarefas['complete'] = $ncontatarefas['complete']+1; }
						if ($listItem->state == 'incomplete'){$ncontatarefas['incomplete'] = $ncontatarefas['incomplete']+1; }
						
					}
				}	
			}
		}
	}
	return $ncontatarefas;
}

function get_nome_quadro($quadro){
	if($quadro == 'todos'){return "Todos";}
	include ('config.php');
	
	$card = trello_get_board($key,$token,$quadro);
	$nome = $card->name;
	return $nome;
}

function trellodashboard_get_json_data($url_base){
	$file = fopen($url_base, "r");
	if (!$file){
		$array = 'null';
	}else{
			while (!feof ($file)) {
			$line =  $line.fgets ($file, 1024);
			}
			
		fclose($file);
		$array2 = json_decode($line);
		$array2 = $array2;	
	}	
return($array2);
}

function trellodashboar_out_esforco($trello){
	
	foreach ($trello['quadro']['cartoes'] as $cartoes_array){
		foreach($cartoes_array as $cartoes){
			foreach($cartoes['checklist'] as $checklist ){
				if (count($checklist['list'])!=0){
					foreach($checklist['list'] as $list){
							foreach($list->checkItems as $listItem){
								$esforco = explode("(",$listItem->name)[1];
								$esforco = str_replace(")","",$esforco);
								if (strstr($esforco, ':') != ""){  
									$esforco_nome = strtoupper (explode(":",$esforco)[0]);  //Converte sempre para maiusculo
									$esforco_medida = strtoupper( substr( explode(":",$esforco)[1] ,-1));
									$esforco_qdt = substr( explode(":",$esforco)[1] ,0,-1);
									$esforco_out[$esforco_nome][$esforco_medida][$listItem->state][] = $esforco_qdt;
								}
							}
					}
				}else{
								//caso não haja nenhum checklist
				}
			}
		}
	}
	
	foreach ($esforco_out as $nome=>$esforco){
		foreach ($esforco as $medida=>$status){
			foreach ($status as $status_value=>$qtd_array){
				foreach ($qtd_array as $qtd){
					$qtd_calc[$nome][$medida][$status_value] = $qtd_calc[$nome][$medida][$status_value] + intval($qtd);
				}
			}
		}
	}
	
	return $qtd_calc;	
}

function trellodashboar_out_esforco_googlechart($trello){
	$esforco = trellodashboar_out_esforco($trello);
	
		foreach ($esforco as $nome=>$medida_array){
			foreach ($medida_array as $medida=>$status_array){
				$value_complete = 0; $value_incomplete = 0;
				foreach ($status_array as $status=>$value){
					if($status=='complete'){$value_complete = $value;}
					if($status=='incomplete'){$value_incomplete = $value;}
				}
				if(!isset($value_incomplete)){$value_incomplete=0;}
				if(!isset($value_complete)){$value_complete=0;}
				
				$out = $out . "['".$nome."',".($value_incomplete+$value_complete).",".$value_complete.",".$value_incomplete."],";
			}
	}
	return $out;
}

function trellodashboar_out_esforco_medida($trello){
	$esforco = trellodashboar_out_esforco($trello);
	
		foreach ($esforco as $nome=>$medida_array){
		foreach ($medida_array as $medida=>$status_array){
			return strtoupper($medida);
		}
	}
}

function trellodashboar_get_prioridade($listItem_name){

	$prioridade = explode("(",$listItem_name)[1];
	$prioridade  = explode (")", $prioridade )[0];
	if (strstr($prioridade, ':') != ""){ 
		$prioridade = explode (":", $prioridade)[2];
		$prioridade = strtoupper($prioridade);
		return $prioridade;
	}else{
		return "P2";
	}
}

function trellodashboar_legenda_prioridade($prioridade){
	include ('config.php');
	foreach ($leg_prioridade as $leg_key=>$leg_value){
		if ($leg_key == $prioridade){return $leg_value;}
	}
	return $prioridade;
}


?>